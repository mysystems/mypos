/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.mypos.dao;

import java.util.List;

/**
 *
 * @author 65160
 */
public interface Dao <T>{
    T get(int id);
    T getByName(String name);
    List<T> getAll();
    List<T> getAll(String where, String order);
    T save(T obj);
    T update(T obj);
    int delete(T obj);
}