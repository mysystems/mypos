/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.dao;

import com.mycompany.mypos.databaseHelper.DatabaseHelper;
import com.mycompany.mypos.model.CheckStock;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class CheckStockDao implements Dao<CheckStock> {

    @Override
    public CheckStock get(int id) {
        CheckStock checkStock = null;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM check_stock WHERE cs_id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkStock = CheckStock.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return checkStock;
    }

    @Override
    public CheckStock getByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CheckStock> getAll() {
        ArrayList<CheckStock> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM check_stock");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckStock checkStock = CheckStock.fromRs(rs);
                list.add(checkStock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public List<CheckStock> getAll(String where, String order) {
        ArrayList<CheckStock> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM check_stock WHERE " + where + " ORDER BY " + order);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckStock checkStock = CheckStock.fromRs(rs);
                list.add(checkStock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public CheckStock save(CheckStock obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO check_stock(cs_date, user_id) VALUES(?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDate(1, (Date) obj.getDate());
            stmt.setInt(2, obj.getUserId());
            stmt.executeUpdate();

            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public CheckStock update(CheckStock obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE check_stock SET cs_date=date('now','localtime'), user_id=? WHERE cs_id=?";

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.setInt(2, obj.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }

    @Override
    public int delete(CheckStock obj) {
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM check_stock WHERE cs_id=?");
            stmt.setInt(1,obj.getId());
            int result = stmt.executeUpdate();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

}
