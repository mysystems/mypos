/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.dao;

import com.mycompany.mypos.databaseHelper.DatabaseHelper;
import com.mycompany.mypos.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class ProductDao implements Dao<Product> {

    @Override
    public Product get(int id) {
        Connection conn = DatabaseHelper.getConnect();
        Product product = null;
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM product WHERE product_id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                product = Product.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

    @Override
    public Product getByName(String name) {
        Connection conn = DatabaseHelper.getConnect();
        Product product = null;
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM product WHERE product_name=?");
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                product = Product.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

    @Override
    public List<Product> getAll() {
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<Product> list = new ArrayList<>();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM product");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Product product = Product.fromRs(rs);
                list.add(product);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public List<Product> getAll(String where, String order) {
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<Product> list = new ArrayList<>();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM product WHERE " + where + " ORDER BY " + order);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Product product = Product.fromRs(rs);
                list.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public Product save(Product obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO product(product_name, product_price, product_type) VALUES(?,?,?)";
        if (obj.getType()>0) {
            sql = "INSERT INTO product(product_name, product_price, product_type, product_qty) VALUES(?,?,?,?)";
        }

        PreparedStatement stmt;
        try {
            if (obj.getType()>0) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setFloat(2, obj.getPrice());
                stmt.setInt(3, obj.getType());
                stmt.setInt(4, obj.getQty());
            } else {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setFloat(2, obj.getPrice());
                stmt.setInt(3, obj.getType());
            }
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public Product update(Product obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE product SET product_name=?, product_price=?, product_type=?, product_qty=NULL WHERE product_id=?";
        if (obj.getType()>0) {
            sql = "UPDATE product SET product_name=?, product_price=?, product_type=?, product_qty=? WHERE product_id=?";
        }
        PreparedStatement stmt;
        try {
            if (obj.getType()>0) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setFloat(2, obj.getPrice());
                stmt.setInt(3, obj.getType());
                stmt.setInt(4, obj.getQty());
                stmt.setInt(5, obj.getId());
                stmt.executeUpdate();
            } else {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, obj.getName());
                stmt.setFloat(2, obj.getPrice());
                stmt.setInt(3, obj.getType());
                stmt.setInt(4, obj.getId()); 
            }
            stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public int delete(Product obj) {
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM product WHERE product_id=?");
            stmt.setInt(1,obj.getId());
            int result = stmt.executeUpdate();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

}
