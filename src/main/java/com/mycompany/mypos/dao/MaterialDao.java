/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.dao;

import com.mycompany.mypos.databaseHelper.DatabaseHelper;
import com.mycompany.mypos.model.Material;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class MaterialDao implements Dao<Material>{

    @Override
    public Material get(int id) {
        Material material = null;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM material WHERE mat_id=?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                material = Material.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return material;
    }

    @Override
    public Material getByName(String name) {
        Material material = null;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM material WHERE mat_name=?");
            stmt.setString(1,name);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                material = Material.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return material;
    }

    @Override
    public List<Material> getAll() {
        ArrayList<Material> list = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM material");
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                Material material = Material.fromRs(rs);
                list.add(material);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public List<Material> getAll(String where, String order) {
         ArrayList<Material> list = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM material WHERE "+where+" ORDER BY "+order);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
                Material material = Material.fromRs(rs);
                list.add(material);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public Material save(Material obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO material(mat_name, mat_price, mat_qty, mat_min, mat_imp_date) VALUES(?,?,?,?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2,obj.getPrice());
            stmt.setInt(3, obj.getQty());
            stmt.setInt(4, obj.getMin());
            stmt.setDate(5, (Date) obj.getImpDate());
            stmt.executeUpdate();
            
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;        
    }

    @Override
    public Material update(Material obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE material SET mat_name=?, mat_price=?, mat_qty=?, mat_min=? WHERE mat_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2,obj.getPrice());
            stmt.setInt(3, obj.getQty());
            stmt.setInt(4, obj.getMin());
            stmt.setInt(5, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(Material obj) {
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM material WHERE mat_id=?");
            stmt.setInt(1,obj.getId());
            int result = stmt.executeUpdate();
            return result;  
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;    
    }
    
}
