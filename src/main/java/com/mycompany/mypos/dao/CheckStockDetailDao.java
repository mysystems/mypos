/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.dao;

import com.mycompany.mypos.databaseHelper.DatabaseHelper;
import com.mycompany.mypos.model.CheckStockDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class CheckStockDetailDao implements Dao<CheckStockDetail> {

    @Override
    public CheckStockDetail get(int id) {
        CheckStockDetail csd = null;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM check_stock_detail WHERE csd_id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                csd = CheckStockDetail.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return csd;
    }

    @Override
    public CheckStockDetail getByName(String name) {
        CheckStockDetail csd = null;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM check_stock_detail WHERE csd_descript=?");
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                csd = CheckStockDetail.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return csd;
    }

    @Override
    public List<CheckStockDetail> getAll() {
        ArrayList<CheckStockDetail> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM check_stock_detail");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckStockDetail csd = CheckStockDetail.fromRs(rs);
                list.add(csd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetailDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public List<CheckStockDetail> getAll(String where, String order) {
        ArrayList<CheckStockDetail> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM check_stock_detail WHERE "+where+" ORDER BY "+order);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckStockDetail csd = CheckStockDetail.fromRs(rs);
                list.add(csd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetailDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public CheckStockDetail save(CheckStockDetail obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO check_stock_detail(csd_descript, csd_qty, mat_id, cs_id) VALUES(?,?,?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDescript());
            stmt.setInt(2, obj.getQty());
            stmt.setInt(3, obj.getMaterialId());
            stmt.setInt(4, obj.getCheckStockId());
            stmt.executeQuery();
            
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public CheckStockDetail update(CheckStockDetail obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE check_stock_detail SET csd_qty=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getQty());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public int delete(CheckStockDetail obj) {
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM check_stock_detail WHERE csd_id=?");
            stmt.setInt(1, obj.getId());
            int result = stmt.executeUpdate();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

}
