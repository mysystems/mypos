/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.dao;

import com.mycompany.mypos.databaseHelper.DatabaseHelper;
import com.mycompany.mypos.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class UserDao implements Dao<User> {

    @Override
    public User get(int id) {
        Connection conn = DatabaseHelper.getConnect();
        User user = null;

        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM user where user_id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRs(rs);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    @Override
    public User getByName(String name) {
        Connection conn = DatabaseHelper.getConnect();
        User user = null;

        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM user where user_name=?");
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRs(rs);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }
    
    public User getByPassword(String password) {
        Connection conn = DatabaseHelper.getConnect();
        User user = null;

        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM user where user_password=?");
            stmt.setString(1, password);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRs(rs);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<User> list = new ArrayList<>();

        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM user");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                User user = User.fromRs(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<User> getAll(String where, String order) {
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<User> list = new ArrayList<>();

        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM user WHERE " + where + " ORDER BY " + order);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                User user = User.fromRs(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public User save(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO user(user_name, user_password, user_type) VALUES(?,?,?)";

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getPassword());
            stmt.setInt(3,obj.getType());
            stmt.executeUpdate();

//            ResultSet key = stmt.getGeneratedKeys();
//            int id = key.getInt(1);
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public User update(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE user SET user_name=?, user_password=?, user_type=? WHERE user_id=?";

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getPassword());
            stmt.setInt(3, obj.getType());
            stmt.setInt(4,obj.getId());

            int result = stmt.executeUpdate();
            System.out.println(result);
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public int delete(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "DELETE FROM user WHERE user_id=?";

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int result = stmt.executeUpdate();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

}
