/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.dao;

import com.mycompany.mypos.databaseHelper.DatabaseHelper;
import com.mycompany.mypos.model.Vendor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class VendorDao implements Dao<Vendor> {

    @Override
    public Vendor get(int id) {
        Vendor vendor = null;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM vendor WHERE vendor_id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                vendor = Vendor.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vendor;

    }

    @Override
    public Vendor getByName(String name) {
        Vendor vendor = null;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM vendor WHERE vendor_name=?");
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                vendor = Vendor.fromRs(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vendor;
    }

    @Override
    public List<Vendor> getAll() {
        ArrayList<Vendor> list = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM vendor");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Vendor vendor = Vendor.fromRs(rs);
                list.add(vendor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public List<Vendor> getAll(String where, String order) {
        ArrayList<Vendor> list = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM vendor WHERE " + where + " ORDER BY " + order);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Vendor vendor = Vendor.fromRs(rs);
                list.add(vendor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return list;
    }

    @Override
    public Vendor save(Vendor obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO vendor(vendor_name) VALUES(?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.executeUpdate();

            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public Vendor update(Vendor obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE vendor SET vendor_name=? WHERE vendor_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }

    @Override
    public int delete(Vendor obj) {
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM vendor WHERE vendor_id=?");
            stmt.setInt(1,obj.getId());
            int result = stmt.executeUpdate();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1; 
    }

}
