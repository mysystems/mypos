/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.mypos.ui;

import com.mycompany.mypos.model.User;
import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author 65160
 */
public class StockPanel extends javax.swing.JPanel {
    /**
     * Creates new form StockPanel
     */
    public StockPanel() {
        initComponents();
        tabbedPane.removeAll();
        tabbedPane.add(new MaterialPanel());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lblMaterial = new javax.swing.JLabel();
        lblCheckStock = new javax.swing.JLabel();
        tabbedPane = new javax.swing.JTabbedPane();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMaterial.setBackground(new java.awt.Color(204, 204, 204));
        lblMaterial.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMaterial.setText("Material");
        lblMaterial.setOpaque(true);
        lblMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblMaterialMouseClicked(evt);
            }
        });
        jPanel3.add(lblMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 6, 130, 40));

        lblCheckStock.setBackground(new java.awt.Color(255, 255, 255));
        lblCheckStock.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCheckStock.setText("Checkstock");
        lblCheckStock.setOpaque(true);
        lblCheckStock.setPreferredSize(new java.awt.Dimension(43, 16));
        lblCheckStock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCheckStockMouseClicked(evt);
            }
        });
        jPanel3.add(lblCheckStock, new org.netbeans.lib.awtextra.AbsoluteConstraints(131, 6, 130, 40));

        add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1020, 50));
        add(tabbedPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 1010, 540));
    }// </editor-fold>//GEN-END:initComponents

    private void lblMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMaterialMouseClicked
        lblMaterial.setBackground(new Color(204,204,204));
        lblCheckStock.setBackground(Color.white);
        tabbedPane.removeAll();
        tabbedPane.add(new MaterialPanel());
    }//GEN-LAST:event_lblMaterialMouseClicked

    private void lblCheckStockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCheckStockMouseClicked
        lblCheckStock.setBackground(new Color(204,204,204));
        lblMaterial.setBackground(Color.white);
        tabbedPane.removeAll();
        tabbedPane.add(new CheckStockPanel());
    }//GEN-LAST:event_lblCheckStockMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblCheckStock;
    private javax.swing.JLabel lblMaterial;
    private javax.swing.JTabbedPane tabbedPane;
    // End of variables declaration//GEN-END:variables
}
