/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class TestTime {
    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);
        String dateTime = "2023-12-03 10:15:44";
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
            System.out.println("Data: "+date);
        } catch (ParseException ex) {
            Logger.getLogger(TestTime.class.getName()).log(Level.SEVERE, null, ex);
        }
        String currentTime = new SimpleDateFormat("HH:mm:ss").format(date);
        System.out.println("CurrentTime: "+currentTime);
    }
}
