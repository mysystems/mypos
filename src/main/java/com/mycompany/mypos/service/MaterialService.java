/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.service;

import com.mycompany.mypos.dao.MaterialDao;
import com.mycompany.mypos.model.Material;
import java.util.ArrayList;

/**
 *
 * @author 65160
 */
public class MaterialService {

    public Material get(int id) {
        MaterialDao matDao = new MaterialDao();
        return matDao.get(id);
    }
    
    public Material getByName(String name){
        MaterialDao matDao = new MaterialDao();
        return matDao.getByName(name);
    }

    public ArrayList<Material> getAll() {
        MaterialDao matDao = new MaterialDao();
        return (ArrayList<Material>) matDao.getAll();
    }

    public ArrayList<Material> getAll(String where, String order) {
        MaterialDao matDao = new MaterialDao();
        return (ArrayList<Material>) matDao.getAll(where, order);
    }

    public Material addNew(Material material) {
        MaterialDao matDao = new MaterialDao();
        return matDao.save(material);
    }

    public Material update(Material material) {
        MaterialDao matDao = new MaterialDao();
        return matDao.update(material);
    }

    public int delete(Material material) {
        MaterialDao matDao = new MaterialDao();
        return matDao.delete(material);
    }
}
