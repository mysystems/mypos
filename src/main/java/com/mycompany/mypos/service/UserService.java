/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.service;

import com.mycompany.mypos.dao.UserDao;
import com.mycompany.mypos.model.User;
import java.util.List;

/**
 *
 * @author 65160
 */
public class UserService {
    
    public User checkLogin(String userName, String password){
        UserDao userDao = new UserDao();
        User user1 = getByName(userName);
        User user2 = getByPassword(password);
        if(user1 == null || user2==null)return null;
            
        if(user1.getId()== user2.getId()){
            return userDao.get(user1.getId());
        }
        return null;
    }
    
    public User get(int id){
        UserDao userDao = new UserDao();
        return userDao.get(id);
    }
    
    public User getByName(String name){
        UserDao userDao = new UserDao();
        return userDao.getByName(name);
    }
    
    public User getByPassword(String password){
        UserDao userDao = new UserDao();
        return userDao.getByPassword(password);
    }
    
    public List<User> getAll(){
        UserDao userDao = new UserDao();
        return userDao.getAll();
    }
    
    public List<User> getAll(String where, String order){
        UserDao userDao = new UserDao();
        return userDao.getAll(where, order);
    }
    
    public User addNew(User user){
        UserDao userDao = new UserDao();
        return userDao.save(user);
    }
    
    public User update(User user){
        UserDao userDao = new UserDao();
        return userDao.update(user);
    }
    
    public int delete(User user){
        UserDao userDao = new UserDao();
        return userDao.delete(user);
    }
}
