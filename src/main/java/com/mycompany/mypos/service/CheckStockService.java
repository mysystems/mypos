/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.service;

import com.mycompany.mypos.dao.CheckStockDao;
import com.mycompany.mypos.model.CheckStock;
import java.util.ArrayList;

/**
 *
 * @author 65160
 */
public class CheckStockService {
    public CheckStock get(int id){
        CheckStockDao csDao = new CheckStockDao();
        return csDao.get(id);
    }
    
    public ArrayList<CheckStock> getAll(){
        CheckStockDao csDao = new CheckStockDao();
        return (ArrayList<CheckStock>) csDao.getAll("cs_id >= 0", "cs_date DESC");
    }
    
    public CheckStock addNew(CheckStock checkStock){
        CheckStockDao csDao = new CheckStockDao();
        return csDao.save(checkStock);
    }
    
    public CheckStock update(CheckStock checkStock){
        CheckStockDao csDao = new CheckStockDao();
        return csDao.update(checkStock);
    }
    
    public int delete(CheckStock checkStock){
        CheckStockDao csDao = new CheckStockDao();
        return csDao.delete(checkStock);
    }
}
