/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.service;

import com.mycompany.mypos.dao.ProductDao;
import com.mycompany.mypos.model.Product;
import java.util.List;

/**
 *
 * @author 65160
 */
public class ProductService {
    public Product get(int id){
        ProductDao productDao = new ProductDao();
        return productDao.get(id);
    }
    
    public Product getByName(String name){
        ProductDao productDao = new ProductDao();
        return productDao.getByName(name);
    }
    
    public List<Product> getAll(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll();
    }
    
    public List<Product> getAll(String where, String order){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(where, order);
    }
    
    public Product addNew(Product product){
        ProductDao productDao = new ProductDao();
        return productDao.save(product);
    }
    
    public Product update(Product product){
        ProductDao productDao = new ProductDao();
        return productDao.update(product);
    }
    
    public int delete(Product product){
        ProductDao productDao = new ProductDao();
        return productDao.delete(product);
    }
}
