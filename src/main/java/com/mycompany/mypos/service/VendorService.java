/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.service;

import com.mycompany.mypos.dao.VendorDao;
import com.mycompany.mypos.model.Vendor;
import java.util.ArrayList;

/**
 *
 * @author 65160
 */
public class VendorService {
    public Vendor get(int id){
        VendorDao vendorDao = new VendorDao();
        return vendorDao.get(id);
    }
    
    public ArrayList<Vendor> getAll(){
        VendorDao vendorDao = new VendorDao();
        return (ArrayList<Vendor>) vendorDao.getAll();
    }
    
    public ArrayList<Vendor> getAll(String where, String order){
        VendorDao vendorDao = new VendorDao();
        return (ArrayList<Vendor>) vendorDao.getAll(where, order);
    }

    public Vendor addNew(Vendor vendor){
        VendorDao vendorDao = new VendorDao();
        return vendorDao.save(vendor);
    }
    
    public Vendor update(Vendor vendor){
        VendorDao vendorDao = new VendorDao();
        return vendorDao.update(vendor);
    }
    
    public int delete(Vendor vendor){
        VendorDao vendorDao = new VendorDao();
        return vendorDao.delete(vendor);
    }
}
