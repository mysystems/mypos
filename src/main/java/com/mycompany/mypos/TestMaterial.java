/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos;

import com.mycompany.mypos.dao.MaterialDao;
import com.mycompany.mypos.model.Material;

/**
 *
 * @author 65160
 */
public class TestMaterial {
    public static void main(String[] args) {
        MaterialDao matDao = new MaterialDao();
        Material mat = matDao.get(1);
        mat.setName("Avocado");
        matDao.update(mat);
    }
}
