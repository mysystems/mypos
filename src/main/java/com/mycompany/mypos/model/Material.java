/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class Material {

    private int id;
    private String name;
    private float price;
    private int qty;
    private int min;
    private Date impDate;

    public Material(int id, String name, float price, int qty, int min, Date impDate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.min = min;
        this.impDate = impDate;
    }

    public Material(String name, float price, int qty, int min) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.min = min;
        this.impDate = null;
    }

    public Material() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.qty = 0;
        this.min = 0;
        this.impDate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public Date getImpDate() {
        return impDate;
    }

    public void setImpDate(Date impDate) {
        this.impDate = impDate;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", price=" + price + ", qty=" + qty + ", min=" + min + ", impDate=" + impDate + '}';
    }

    public static Material fromRs(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("mat_id"));
            material.setName(rs.getString("mat_name"));
            material.setPrice(rs.getFloat("mat_price"));
            material.setQty(rs.getInt("mat_qty"));
            material.setMin(rs.getInt("mat_min"));
            try {
                if (rs.getString("mat_imp_date") != null) {
                    material.setImpDate(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("mat_imp_date")));
                }else{
                    material.setImpDate(null);
                }
            } catch (ParseException ex) {
                Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }

}
