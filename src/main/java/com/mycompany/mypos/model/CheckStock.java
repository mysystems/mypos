/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.model;

import com.mycompany.mypos.dao.CheckStockDetailDao;
import com.mycompany.mypos.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class CheckStock {

    private int id;
    private Date date;
    private int userId;
    private User user;
    private ArrayList<CheckStockDetail> checkStockDetails = new ArrayList();

    public CheckStock(int id, Date date, int userId) {
        this.id = id;
        this.date = date;
        this.userId = userId;
    }

    public CheckStock(Date date, int userId) {
        this.id = -1;
        this.date = date;
        this.userId = userId;
    }

    public CheckStock() {
        this.id = -1;
        this.date = null;
        this.userId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public ArrayList<CheckStockDetail> getCheckStockDetails() {
        return checkStockDetails;
    }

    public void setCheckStockDetails(ArrayList<CheckStockDetail> checkStockDetails) {
        this.checkStockDetails = checkStockDetails;     
    }

    @Override
    public String toString() {
        return "CheckStock{" + "id=" + id + ", date=" + date + ", userId=" + userId + ", user=" + user + ", checkStockDetails=" + checkStockDetails + '}';
    }

    

    public static CheckStock fromRs(ResultSet rs) {
        CheckStock checkStock = new CheckStock();
        try {
            checkStock.setId(rs.getInt("cs_id"));
            try {
                checkStock.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("cs_date")));
            } catch (ParseException ex) {
                Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            }
            checkStock.setUserId(rs.getInt("user_id"));
            
            

            //Populate
            UserDao userDao = new UserDao();
            
            User user = userDao.get(checkStock.getUserId());
            checkStock.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStock;
    }

}
