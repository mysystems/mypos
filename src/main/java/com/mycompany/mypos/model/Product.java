/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class Product {
    private int id;
    private String name;
    private float price;
    private int qty;
    private int type;

    public Product(int id, String name, float price, int type, int qty) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.type = type;
    }
    
    public Product(String name, float price, int type, int qty ) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.type = type;
    }
    
    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.qty = 0;
        this.type = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", type=" + type +", qty=" + qty + '}';
    }
    
    public static Product fromRs(ResultSet rs){
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setPrice(rs.getFloat("product_price"));
            product.setType(rs.getInt("product_type"));
            product.setQty(rs.getInt("product_qty"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
    
    

    
    
    
    
}
