/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mypos.model;

import com.mycompany.mypos.dao.CheckStockDao;
import com.mycompany.mypos.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class CheckStockDetail {
    private int id;
    private String descript;
    private int qty;
    private int materialId;
    private int checkStockId;
    private Material material;
    private CheckStock checkStock;

    public CheckStockDetail(int id, String descript, int qty, int materialId, int checkStockId) {
        this.id = id;
        this.descript = descript;
        this.qty = qty;
        this.materialId = materialId;
        this.checkStockId = checkStockId;
    }
    
    public CheckStockDetail(String descript, int qty, int materialId, int checkStockId) {
        this.id = -1;
        this.descript = descript;
        this.qty = qty;
        this.materialId = materialId;
        this.checkStockId = checkStockId;
    }
    
    public CheckStockDetail() {
        this.id = -1;
        this.descript = "";
        this.qty = 0;
        this.materialId = -1;
        this.checkStockId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public int getCheckStockId() {
        return checkStockId;
    }

    public void setCheckStockId(int checkStockId) {
        this.checkStockId = checkStockId;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
        this.materialId = material.getId();
    }

    public CheckStock getCheckStock() {
        return checkStock;
    }

    public void setCheckStock(CheckStock checkStock) {
        this.checkStock = checkStock;
        this.checkStockId = checkStock.getId(); 
    }
    
    

    @Override
    public String toString() {
        return "CheckStockDetail{" + "id=" + id + ", descript=" + descript + ", qty=" + qty + ", materialId=" + materialId + ", checkStockId=" + checkStockId + ", material=" + material + ", checkStock=" + checkStock + '}';
    }
    
    public static CheckStockDetail fromRs(ResultSet rs){
        CheckStockDetail checkStockDetail = new CheckStockDetail();
        
        try {
            checkStockDetail.setId(rs.getInt("csd_id"));
            checkStockDetail.setDescript(rs.getString("csd_descript"));
            checkStockDetail.setQty(rs.getInt("csd_qty"));
            checkStockDetail.setMaterialId(rs.getInt("mat_id"));
            checkStockDetail.setCheckStockId(rs.getInt("cs_id"));
            
            //Populate
            MaterialDao matDao = new MaterialDao();
            CheckStockDao csDao = new CheckStockDao();
            
            Material material = matDao.get(checkStockDetail.getMaterialId());
            CheckStock checkStock = csDao.get(checkStockDetail.getCheckStockId());
            
            checkStockDetail.setCheckStock(checkStock);
            checkStockDetail.setMaterial(material);
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStockDetail;
    }
    
    
    
    
    
    
}
