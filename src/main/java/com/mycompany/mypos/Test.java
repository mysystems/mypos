/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mypos;

import com.mycompany.mypos.dao.UserDao;
import com.mycompany.mypos.model.User;

/**
 *
 * @author 65160
 */
public class Test {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        User user = userDao.get(2);
        user.setName("Muzashi");
        userDao.update(user);
    }
}
